$(window).scroll(function() {
	var scroll = $(window).scrollTop();

	if (scroll >= 50) {
			$("nav").addClass("scrolling");
	} else {
			$("nav").removeClass("scrolling");
	}

	if (scroll >= 500) {
			$("#btnmobile").addClass("in");
	} else {
			$("#btnmobile").removeClass("in");
	}

	$("#mainDiv").bind("divResized", function(){
			var height = $(this).height();
			$("#secondDiv").height(height);
	});

 	var heightTop = $(".showcase-intern").height();

	if (scroll >= heightTop + 73) {
			$(".sticky").addClass("fixed");
			console.log(heightTop);
	} else {
			$(".sticky").removeClass("fixed");
	}

	var heightTopBoxFixed = $(".container-details").height() + $(".showcase-intern").height() + 120;
	if (scroll >= heightTopBoxFixed + 92) {
			$(".box-fixed").addClass("fixed");
			console.log(heightTop);
	} else {
			$(".box-fixed").removeClass("fixed");
	}

});

$(document).ready(function() {

	$('#CarouselProducts').carousel({
	                interval: false
	        });

	        //Handles the carousel thumbnails
	        $('[id^=carousel-selector-]').click(function () {
	        var id_selector = $(this).attr("id");
	        try {
	            var id = /-(\d+)$/.exec(id_selector)[1];
	            console.log(id_selector, id);
	            jQuery('#CarouselProducts').carousel(parseInt(id));
	        } catch (e) {
	            console.log('Regex failed!', e);
	        }
	    });
	        // When the carousel slides, auto update the text
	        $('#CarouselProducts').on('slid.bs.carousel', function (e) {
	                 var id = $('.item.active').data('slide-number');
	                $('#carousel-text').html($('#slide-content-'+id).html());
	        });

	$(".sticky").css({
    'width': ($(".col-md-3").width() + 'px')
  });

	$(".box-fixed").css({
    'width': ($(".col-md-3").width() + 'px')
  });

	    // Smooth scrolling at click on nav menu item
	    $('a[href*=#]:not([href=#])').click(function() {
	        var target = $(this.hash);
	        $('html,body').animate({
	            scrollTop: target.offset().top - offset
	        }, 500);
	        return false;
	    });


	$('.hamburger').on('click', function(){
		$(this).toggleClass('open');
		$('.list-menu-container').toggleClass('open');
		$('.navbar').toggleClass('open');
	});


});


$(window).scroll(function() {
		var scrollDistance = $(window).scrollTop();

		// Assign active class to nav links while scolling
		$('.page-content-section').each(function(i) {
				if ($(this).position().top + 100 <= scrollDistance) {
						$('.navigation a.active').removeClass('active');
						$('.navigation a').eq(i).addClass('active');
				}
		});
}).scroll();
